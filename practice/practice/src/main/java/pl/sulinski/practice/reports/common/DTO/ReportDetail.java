package pl.sulinski.practice.reports.common.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class ReportDetail {
    private String name;
    private BigDecimal totalCost;
    private BigDecimal totalTime;
}
