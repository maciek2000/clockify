package pl.sulinski.practice.reports.project.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sulinski.practice.common.exception.AccessDeniedException;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.project.service.ProjectService;
import pl.sulinski.practice.record.repository.RecordRepository;
import pl.sulinski.practice.reports.common.DTO.ReportDetail;
import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.common.service.AbstractReportService;
import pl.sulinski.practice.reports.project.DTO.ProjectReportDTO;
import pl.sulinski.practice.reports.project.DTO.ProjectReportProjection;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectReportServiceImpl extends AbstractReportService implements ProjectReportService {

    private final RecordRepository recordRepository;
    private final ProjectService projectService;


    @Override
    public ProjectReportDTO searchProjectData(final UUID managerUuid, final UUID projectUuid, final TimePeriod timePeriod) {
        if (!projectService.hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }

        final LocalDateTime lowerDateRange = getLowerDateRange(timePeriod).atStartOfDay();
        final LocalDateTime upperDateRange = getUpperDateRange(timePeriod).atStartOfDay();
        final Project project = projectService.findProject(projectUuid);

        final List<ProjectReportProjection> queryResult = recordRepository.findByProjectUuidAndDateRange(projectUuid,
                lowerDateRange, upperDateRange);

        final ProjectReportDTO projectReportDTO = new ProjectReportDTO();

        queryResult.forEach(user -> calculateTotalProjectTimeAndCost(user, projectReportDTO));
        final BigDecimal Hundred = BigDecimal.valueOf(100);

        projectReportDTO.setOverBudget(project.getPercentageBudgetUsage().compareTo(Hundred) > 0);

        return projectReportDTO;
    }

    private void calculateTotalProjectTimeAndCost(final ProjectReportProjection user, final ProjectReportDTO projectReportDTO) {
        projectReportDTO.setSummaryCost(projectReportDTO.getSummaryCost().add(user.getCost()));
        projectReportDTO.setSummaryTime(projectReportDTO.getSummaryTime().add(user.getHours()));
        projectReportDTO.getProjectUsers().add(new ReportDetail(user.getLogin(), user.getCost(), user.getHours()));
    }
}