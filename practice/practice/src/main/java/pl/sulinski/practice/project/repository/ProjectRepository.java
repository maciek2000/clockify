package pl.sulinski.practice.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import pl.sulinski.practice.project.entity.Project;

import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project> {

    boolean existsByName(final String name);

    Optional<Project> findByUuid(final UUID uuid);

    @Query("SELECT p FROM Project p " +
            "LEFT JOIN FETCH p.users u " +
            "WHERE p.uuid = :uuid " +
            "AND u.uuid = :userUuid")
    Optional<Project> findByUuidAndUsersUuid(final UUID uuid, final UUID userUuid);
}
