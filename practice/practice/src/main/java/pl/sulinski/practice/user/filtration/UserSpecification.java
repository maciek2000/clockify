package pl.sulinski.practice.user.filtration;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import pl.sulinski.practice.common.specification.SpecificationUtils;
import pl.sulinski.practice.user.entity.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class UserSpecification implements Specification<User> {

    private UserFilterForm userFilterForm;

    @Override
    public Predicate toPredicate(final Root<User> root, final CriteriaQuery<?> query,
                                 final CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicateList = new ArrayList<>();
        SpecificationUtils.isLike(criteriaBuilder, root.get(User.Fields.login), userFilterForm.getLogin(), predicateList);
        SpecificationUtils.isLike(criteriaBuilder, root.get(User.Fields.name), userFilterForm.getName(), predicateList);
        SpecificationUtils.isLike(criteriaBuilder, root.get(User.Fields.lastName), userFilterForm.getLastName(), predicateList);
        SpecificationUtils.isBetween(criteriaBuilder, root.get(User.Fields.costPerHour), predicateList, userFilterForm.getLowBound(),
                userFilterForm.getTopBound());
        SpecificationUtils.isEqual(criteriaBuilder, root.get(User.Fields.workerType), predicateList, userFilterForm.getWorkerType());

        return criteriaBuilder.and(predicateList.toArray(Predicate[]::new));
    }

}
