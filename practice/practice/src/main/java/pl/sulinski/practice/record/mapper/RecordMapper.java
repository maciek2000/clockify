package pl.sulinski.practice.record.mapper;

import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.record.DTO.AddRecordForm;
import pl.sulinski.practice.record.DTO.RecordDTO;
import pl.sulinski.practice.record.entity.Record;
import pl.sulinski.practice.user.entity.User;

public class RecordMapper {

    public static Record toRecordFromAddRecordForm(final AddRecordForm addRecordForm, final Project project,
                                                   final User user) {
        return Record.builder()
                .startTime(addRecordForm.startTime())
                .endTime(addRecordForm.endTime())
                .taskDescription(addRecordForm.taskDescription())
                .project(project)
                .user(user)
                .build();
    }

    public static RecordDTO toDto(final Record record) {
        return RecordDTO.builder()
                .startTime(record.getStartTime())
                .endTime(record.getEndTime())
                .taskDescription(record.getTaskDescription())
                .recordCost(record.getRecordCost())
                .userLogin(record.getUser().getLogin())
                .projectName(record.getProject().getName())
                .build();
    }
}

