package pl.sulinski.practice.project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sulinski.practice.project.DTO.AddProjectForm;
import pl.sulinski.practice.project.DTO.ProjectDTO;
import pl.sulinski.practice.project.DTO.UpdateProjectForm;
import pl.sulinski.practice.project.filtration.ProjectFilterForm;
import pl.sulinski.practice.project.service.ProjectService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/project")
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping("/all")
    public List<ProjectDTO> findAll(@RequestParam final UUID managerUuid) {
        return projectService.findAll(managerUuid);
    }

    @PostMapping
    public ProjectDTO saveProject(@RequestParam final UUID managerUuid,
                                  @RequestBody @Valid final AddProjectForm addProjectForm) {
        return projectService.saveProject(managerUuid, addProjectForm);
    }

    @PutMapping("/add")
    public String adduserToProject(@RequestParam final UUID managerUuid, @RequestParam final UUID projectUuid,
                                   @RequestParam final UUID userUuid) {
        return projectService.addUserToProject(managerUuid, projectUuid, userUuid);
    }

    @PutMapping("/delete")
    public String deleteUserFromProject(@RequestParam final UUID managerUuid, @RequestParam final UUID projectUuid,
                                        @RequestParam final UUID userUuid) {
        return projectService.deleteUserFromProject(managerUuid, projectUuid, userUuid);
    }

    @PutMapping("/update")
    public ProjectDTO updateProject(@RequestParam final UUID managerUuid, @RequestParam final UUID projectUuid,
                                    @RequestBody @Valid final UpdateProjectForm updateProjectForm) {
        return projectService.updateProject(managerUuid, projectUuid, updateProjectForm);
    }

    @GetMapping("/search")
    public List<ProjectDTO> searchUsers(@RequestParam final UUID managerUuid, final ProjectFilterForm projectFilterForm) {
        return projectService.searchProjects(managerUuid, projectFilterForm);
    }

}
