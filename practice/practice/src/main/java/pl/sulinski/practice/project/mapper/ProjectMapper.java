package pl.sulinski.practice.project.mapper;

import pl.sulinski.practice.project.DTO.AddProjectForm;
import pl.sulinski.practice.project.DTO.ProjectDTO;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.user.mapper.UserMapper;

import java.util.stream.Collectors;

public class ProjectMapper {

    public static ProjectDTO toDto(final Project project) {
        return ProjectDTO.builder()
                .name(project.getName())
                .description(project.getDescription())
                .durationFrom(project.getDurationFrom())
                .durationTo(project.getDurationTo())
                .budget(project.getBudget())
                .users(project
                        .getUsers()
                        .stream()
                        .map(UserMapper::toDto)
                        .collect(Collectors.toSet()))
                .build();
    }

    public static Project toProjectFromAddProjectForm(final AddProjectForm addProjectForm) {
        return Project.builder()
                .name(addProjectForm.name())
                .description(addProjectForm.description())
                .durationFrom(addProjectForm.durationFrom())
                .durationTo(addProjectForm.durationTo())
                .budget(addProjectForm.budget())
                .build();
    }
}
