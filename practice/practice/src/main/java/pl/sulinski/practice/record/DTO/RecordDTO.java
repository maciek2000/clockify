package pl.sulinski.practice.record.DTO;

import lombok.Builder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
public record RecordDTO(
        LocalDateTime startTime,
        LocalDateTime endTime,
        String taskDescription,
        BigDecimal recordCost,
        String userLogin,
        String projectName) {
}
