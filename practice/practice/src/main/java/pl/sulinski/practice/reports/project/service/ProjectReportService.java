package pl.sulinski.practice.reports.project.service;

import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.project.DTO.ProjectReportDTO;

import java.util.UUID;

public interface ProjectReportService {

    ProjectReportDTO searchProjectData(UUID managerUuid, UUID userUuid, TimePeriod timePeriod);
}
