package pl.sulinski.practice.user.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.user.DTO.AddUserForm;
import pl.sulinski.practice.user.DTO.UpdateUserForm;
import pl.sulinski.practice.user.DTO.UserDTO;
import pl.sulinski.practice.user.filtration.UserFilterForm;
import pl.sulinski.practice.user.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public UserDTO saveUser(@RequestParam final UUID adminUuid,
                            @RequestBody @Valid final AddUserForm userForm) throws NotFoundException {
        return userService.saveUser(adminUuid, userForm);
    }

    @GetMapping("/all")
    public List<UserDTO> findAll(@RequestParam final UUID adminUuid) throws NotFoundException {
        return userService.findAll(adminUuid);
    }

    @DeleteMapping
    public UserDTO deleteUserByUuid(@RequestParam final UUID adminUuid,
                                    @RequestParam final UUID uuid) throws NotFoundException {
        return userService.deleteUserByUuid(adminUuid, uuid);
    }

    @PutMapping
    public UserDTO updateUser(@RequestParam final UUID adminUuid, @RequestParam final UUID uuid,
                              @RequestBody @Valid final UpdateUserForm userForm)
            throws NotFoundException {
        return userService.updateUser(adminUuid, uuid, userForm);
    }

    @GetMapping("/search")
    public List<UserDTO> searchUsers(@RequestParam final UUID adminUuid, final UserFilterForm userFilterForm) {
        return userService.searchUsers(adminUuid, userFilterForm);
    }
}
