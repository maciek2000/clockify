package pl.sulinski.practice.project.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sulinski.practice.common.exception.AccessDeniedException;
import pl.sulinski.practice.common.exception.AlreadyExistsException;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.project.DTO.AddProjectForm;
import pl.sulinski.practice.project.DTO.ProjectDTO;
import pl.sulinski.practice.project.DTO.UpdateProjectForm;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.project.filtration.ProjectFilterForm;
import pl.sulinski.practice.project.filtration.ProjectSpecification;
import pl.sulinski.practice.project.mapper.ProjectMapper;
import pl.sulinski.practice.project.repository.ProjectRepository;
import pl.sulinski.practice.record.entity.Record;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.entity.WorkerType;
import pl.sulinski.practice.user.service.UserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final UserService userService;

    @Override
    public ProjectDTO saveProject(final UUID managerUuid, final AddProjectForm addProjectForm) {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }
        if (projectRepository.existsByName(addProjectForm.name())) {
            throw new AlreadyExistsException("Project with given name already exists: " + addProjectForm.name());
        }

        final Project project = ProjectMapper.toProjectFromAddProjectForm(addProjectForm);
        projectRepository.save(project);
        return ProjectMapper.toDto(project);
    }

    @Override
    public List<ProjectDTO> findAll(final UUID managerUuid) throws NotFoundException {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }
        return projectRepository.findAll()
                .stream()
                .map(project -> {
                    final ProjectDTO projectDTO = ProjectMapper.toDto(project);
                    return projectDTO.setPercentageBudgetUsage(project.getPercentageBudgetUsage());
                })
                .toList();
    }

    @Override
    public String deleteUserFromProject(final UUID managerUuid, final UUID projectUuid,
                                        final UUID userUuid) {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }

        final Project project = findProject(projectUuid);
        final User user = userService.findUser(userUuid);

        project.removeUser(user);
        projectRepository.save(project);
        return "User " + user.getUuid() + " deleted from project " + project.getUuid();
    }

    @Override
    public String addUserToProject(final UUID managerUuid, final UUID projectUuid,
                                   final UUID userUuid) {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }

        final Project project = findProject(projectUuid);
        final User user = userService.findUser(userUuid);

        project.addUser(user);
        projectRepository.save(project);
        return "User " + user.getUuid() + " added to project " + project.getUuid();
    }

    @Override
    public ProjectDTO updateProject(final UUID managerUuid, final UUID projectUuid,
                                    final UpdateProjectForm updateProjectForm) {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }
        final Project project = findProject(projectUuid);

        if (!updateProjectForm.name().equals(project.getName())) {
            if (projectRepository.existsByName(updateProjectForm.name())) {
                throw new AlreadyExistsException("Project with given name already exists: " + updateProjectForm.name());
            }
        }

        project.setName(updateProjectForm.name());
        project.setDescription(updateProjectForm.description());
        project.setDurationFrom(updateProjectForm.durationFrom());
        project.setDurationTo(updateProjectForm.durationTo());
        project.setBudget(updateProjectForm.budget());

        projectRepository.save(project);
        return ProjectMapper.toDto(project);
    }

    @Override
    public List<ProjectDTO> searchProjects(final UUID managerUuid, final ProjectFilterForm projectFilterForm) {
        if (!hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }

        return projectRepository.findAll(new ProjectSpecification(projectFilterForm))
                .stream()
                .map(project -> {
                    final ProjectDTO projectDTO = ProjectMapper.toDto(project);
                    return projectDTO.setPercentageBudgetUsage(project.getPercentageBudgetUsage());
                })
                .distinct()
                .toList();
    }

    @Override
    public boolean hasRoleManager(final UUID uuid) throws NotFoundException {
        final User user = userService.findUser(uuid);
        return user.getWorkerType().equals(WorkerType.MANAGER);
    }

    @Override
    public Project findProject(final UUID projectUuid) {
        return projectRepository.findByUuid(projectUuid)
                .orElseThrow(() -> new NotFoundException("Project not found, uuid : " + projectUuid));
    }

    public BigDecimal calculatePercentageBudgetUsage(final Project project) {
        BigDecimal totalBudgetUsage = BigDecimal.ZERO;
        for (final Record record : project.getRecords()) {
            totalBudgetUsage = totalBudgetUsage.add(record.getRecordCost());
        }
        return (totalBudgetUsage.multiply(BigDecimal.valueOf(100)))
                .divide(project.getBudget(), 2, RoundingMode.HALF_UP);
    }
}
