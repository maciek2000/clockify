package pl.sulinski.practice.user.mapper;

import pl.sulinski.practice.user.DTO.AddUserForm;
import pl.sulinski.practice.user.DTO.UserDTO;
import pl.sulinski.practice.user.entity.User;

public class UserMapper {
    public static UserDTO toDto(final User user) {
        return UserDTO.builder()
                .login(user.getLogin())
                .name(user.getName())
                .lastName(user.getLastName())
                .workerType(user.getWorkerType())
                .password(user.getPassword())
                .email(user.getEmail())
                .costPerHour(user.getCostPerHour())
                .build();
    }

    public static User toUserFromAddUserForm(final AddUserForm userForm) {
        return User.builder()
                .login(userForm.login())
                .name(userForm.name())
                .lastName(userForm.lastName())
                .workerType(userForm.workerType())
                .password(userForm.password())
                .email(userForm.email())
                .costPerHour(userForm.costPerHour())
                .build();
    }
}