package pl.sulinski.practice.reports.user.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.user.DTO.UserReportDTO;
import pl.sulinski.practice.reports.user.service.UserReportService;

import java.util.UUID;

@RestController
@RequestMapping("/api/report")
@RequiredArgsConstructor
public class UserReportController {

    private final UserReportService userReportService;

    @GetMapping("/user")
    public UserReportDTO searchUserData(@RequestParam final UUID managerUuid,
                                        @RequestParam final UUID userUuid,
                                        @RequestParam final TimePeriod timePeriod) {

        return userReportService.searchUserData(managerUuid, userUuid, timePeriod);
    }
}
