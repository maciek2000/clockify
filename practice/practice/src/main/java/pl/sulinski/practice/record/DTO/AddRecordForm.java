package pl.sulinski.practice.record.DTO;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public record AddRecordForm(
        @NotNull
        @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
        LocalDateTime startTime,
        @NotNull
        @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
        LocalDateTime endTime,
        @Nullable
        String taskDescription
) {
}
