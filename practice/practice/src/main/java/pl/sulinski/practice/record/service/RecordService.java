package pl.sulinski.practice.record.service;

import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.record.DTO.AddRecordForm;
import pl.sulinski.practice.record.DTO.RecordDTO;
import pl.sulinski.practice.record.DTO.UpdateRecordForm;
import pl.sulinski.practice.record.entity.Record;

import java.util.List;
import java.util.UUID;

public interface RecordService {

    RecordDTO saveRecord(final UUID projectUuid, final UUID userUuid,
                         final AddRecordForm addRecordForm) throws NotFoundException;

    RecordDTO deleteRecord(final UUID recordUuid, final UUID userUuid) throws NotFoundException;

    List<RecordDTO> findAll(final UUID userUuid) throws NotFoundException;

    RecordDTO updateRecord(final UUID recordUuid, final UUID userUuid,
                           final UpdateRecordForm updateRecordForm) throws NotFoundException;

    Record findRecordByUuidAndUserUuid(final UUID recordUuid, final UUID userUuid);
}


