package pl.sulinski.practice.common.exception;

import java.util.UUID;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException(final UUID uuid) {
        super("No needed role for user: " + uuid);
    }
}
