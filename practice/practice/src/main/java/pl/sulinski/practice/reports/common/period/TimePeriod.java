package pl.sulinski.practice.reports.common.period;

public enum TimePeriod {
    TOTAL, YEAR, MONTH, WEEK
}
