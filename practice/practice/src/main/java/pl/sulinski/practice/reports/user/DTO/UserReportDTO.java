package pl.sulinski.practice.reports.user.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sulinski.practice.reports.common.DTO.ReportDetail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UserReportDTO {
    private BigDecimal summaryCost = BigDecimal.ZERO;
    private BigDecimal summaryTime = BigDecimal.ZERO;
    private List<ReportDetail> userProjects = new ArrayList<>();

}
