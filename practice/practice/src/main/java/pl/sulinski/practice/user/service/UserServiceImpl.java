package pl.sulinski.practice.user.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sulinski.practice.common.exception.AccessDeniedException;
import pl.sulinski.practice.common.exception.AlreadyExistsException;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.user.DTO.AddUserForm;
import pl.sulinski.practice.user.DTO.UpdateUserForm;
import pl.sulinski.practice.user.DTO.UserDTO;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.entity.WorkerType;
import pl.sulinski.practice.user.filtration.UserFilterForm;
import pl.sulinski.practice.user.filtration.UserSpecification;
import pl.sulinski.practice.user.mapper.UserMapper;
import pl.sulinski.practice.user.repository.UserRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDTO saveUser(final UUID adminUuid, final AddUserForm userForm) throws NotFoundException {
        if (!hasRoleAdmin(adminUuid)) {
            throw new AccessDeniedException(adminUuid);
        }
        if (userRepository.existsByLogin(userForm.login())) {
            throw new AlreadyExistsException("Login already exists, uuid: " + adminUuid);
        }
        if (userRepository.existsByEmail(userForm.email())) {
            throw new AlreadyExistsException("Email already exists, uuid: " + adminUuid);
        }
        final User user = UserMapper.toUserFromAddUserForm(userForm);
        userRepository.save(user);
        return UserMapper.toDto(user);
    }

    @Override
    public List<UserDTO> findAll(final UUID adminUuid) throws NotFoundException {
        if (!hasRoleAdmin(adminUuid)) {
            throw new AccessDeniedException(adminUuid);
        }
        return userRepository.findAll().stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO deleteUserByUuid(final UUID adminUuid, final UUID uuid) throws NotFoundException {
        if (!hasRoleAdmin(adminUuid)) {
            throw new AccessDeniedException(adminUuid);
        }

        final User user = findUser(uuid);
        userRepository.deleteById(user.getId());
        return UserMapper.toDto(user);
    }

    @Override
    public UserDTO updateUser(final UUID adminUuid, final UUID uuid, final UpdateUserForm userForm) throws NotFoundException {
        if (!hasRoleAdmin(adminUuid)) {
            throw new AccessDeniedException(adminUuid);
        }

        final User userDB = findUser(uuid);

        if (!userDB.getEmail().equals(userForm.email())) {
            if (userRepository.existsByEmail(userForm.email())) {
                throw new AlreadyExistsException("Email already exists, uuid: " + adminUuid);
            }
        }
        if (!userDB.getLogin().equals(userForm.login())) {
            if (userRepository.existsByLogin(userForm.login())) {
                throw new AlreadyExistsException("Login already exists, uuid: " + adminUuid);
            }
        }

        userDB.setLogin(userForm.login());
        userDB.setName(userForm.name());
        userDB.setLastName(userForm.lastName());
        userDB.setWorkerType(userForm.workerType());
        userDB.setPassword(userForm.password());
        userDB.setEmail(userForm.email());
        userDB.setCostPerHour(userForm.costPerHour());
        userRepository.save(userDB);
        return UserMapper.toDto(userDB);
    }

    @Override
    public boolean hasRoleAdmin(final UUID uuid) throws NotFoundException {
        final User user = findUser(uuid);
        return user.getWorkerType().equals(WorkerType.ADMINISTRATOR);
    }

    @Override
    public List<UserDTO> searchUsers(final UUID adminUuid, final UserFilterForm userFilterForm) {
        if (!hasRoleAdmin(adminUuid)) {
            throw new AccessDeniedException(adminUuid);
        }
        return userRepository.findAll(new UserSpecification(userFilterForm)).stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public User findUser(final UUID userUuid) {
        return userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new NotFoundException("User not found, uuid: " + userUuid));
    }


}
