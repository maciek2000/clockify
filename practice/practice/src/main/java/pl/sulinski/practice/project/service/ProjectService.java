package pl.sulinski.practice.project.service;

import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.project.DTO.AddProjectForm;
import pl.sulinski.practice.project.DTO.ProjectDTO;
import pl.sulinski.practice.project.DTO.UpdateProjectForm;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.project.filtration.ProjectFilterForm;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface ProjectService {
    ProjectDTO saveProject(final UUID managerUuid, final AddProjectForm addProjectForm);

    List<ProjectDTO> findAll(final UUID managerUuid) throws NotFoundException;

    String deleteUserFromProject(final UUID managerUuid, final UUID projectUuid,
                                 final UUID userUuid);

    String addUserToProject(final UUID managerUuid, final UUID projectUuid,
                            final UUID userUuid);

    ProjectDTO updateProject(final UUID managerUuid, final UUID projectUuid,
                             final UpdateProjectForm updateProjectForm);

    boolean hasRoleManager(final UUID uuid) throws NotFoundException;

    List<ProjectDTO> searchProjects(final UUID managerUuid, final ProjectFilterForm projectFilterForm);

    Project findProject(final UUID projectUuid);

    BigDecimal calculatePercentageBudgetUsage(final Project project);
}
