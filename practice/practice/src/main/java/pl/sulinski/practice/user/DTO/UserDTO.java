package pl.sulinski.practice.user.DTO;

import lombok.Builder;
import pl.sulinski.practice.user.entity.WorkerType;

import java.math.BigDecimal;

@Builder
public record UserDTO(
        String login,
        String name,
        String lastName,
        WorkerType workerType,
        String password,
        String email,
        BigDecimal costPerHour) {
}
