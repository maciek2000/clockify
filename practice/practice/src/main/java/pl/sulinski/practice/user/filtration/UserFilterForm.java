package pl.sulinski.practice.user.filtration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sulinski.practice.user.entity.WorkerType;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class UserFilterForm {
    private String login;
    private String name;
    private String lastName;
    private WorkerType workerType;
    private BigDecimal lowBound;
    private BigDecimal topBound;
}
