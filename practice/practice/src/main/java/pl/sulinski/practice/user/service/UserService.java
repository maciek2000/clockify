package pl.sulinski.practice.user.service;

import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.user.DTO.AddUserForm;
import pl.sulinski.practice.user.DTO.UpdateUserForm;
import pl.sulinski.practice.user.DTO.UserDTO;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.filtration.UserFilterForm;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserDTO saveUser(final UUID adminUuid, final AddUserForm userForm) throws NotFoundException;

    List<UserDTO> findAll(final UUID adminUuid) throws NotFoundException;

    UserDTO deleteUserByUuid(final UUID adminUuid, final UUID uuid) throws NotFoundException;

    UserDTO updateUser(final UUID adminUuid, final UUID uuid, final UpdateUserForm userForm) throws NotFoundException;

    boolean hasRoleAdmin(UUID uuid) throws NotFoundException;

    List<UserDTO> searchUsers(final UUID adminUuid, final UserFilterForm userFilterForm);

    User findUser(final UUID userUuid);
}
