package pl.sulinski.practice.project.DTO;

import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;

public record UpdateProjectForm(
        @NotBlank(message = "name cannot be blank")
        String name,
        @Nullable
        String description,
        @NotNull
        LocalDate durationFrom,
        @NotNull
        LocalDate durationTo,
        @NotNull
        @Positive
        @Max(99999999)
        BigDecimal budget) {
}
