package pl.sulinski.practice.reports.common.service;

import pl.sulinski.practice.reports.common.period.TimePeriod;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public abstract class AbstractReportService {

    public LocalDate getLowerDateRange(final TimePeriod timePeriod) {
        final LocalDate now = LocalDate.now();
        return switch (timePeriod) {
            case TOTAL -> LocalDate.EPOCH;
            case YEAR -> now.minusYears(1).with(TemporalAdjusters.firstDayOfYear());
            case MONTH -> now.minusMonths(1).with(TemporalAdjusters.firstDayOfMonth());
            case WEEK -> now.minusWeeks(1).with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
        };
    }

    public LocalDate getUpperDateRange(final TimePeriod timePeriod) {
        final LocalDate now = LocalDate.now();
        return switch (timePeriod) {
            case TOTAL -> now;
            case YEAR -> now.minusYears(1).with(TemporalAdjusters.lastDayOfYear());
            case MONTH -> now.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
            case WEEK -> now.minusWeeks(1).with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
        };
    }
}
