package pl.sulinski.practice.reports.project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.project.DTO.ProjectReportDTO;
import pl.sulinski.practice.reports.project.service.ProjectReportService;

import java.util.UUID;

@RestController
@RequestMapping("/api/report")
@RequiredArgsConstructor
public class ProjectReportController {

    private final ProjectReportService projectReportService;

    @GetMapping("/project")
    public ProjectReportDTO searchProjectData(@RequestParam final UUID managerUuid,
                                              @RequestParam final UUID projectUuid,
                                              @RequestParam final TimePeriod timePeriod) {

        return projectReportService.searchProjectData(managerUuid, projectUuid, timePeriod);
    }
}
