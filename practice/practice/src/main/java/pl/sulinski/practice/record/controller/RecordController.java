package pl.sulinski.practice.record.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sulinski.practice.record.DTO.AddRecordForm;
import pl.sulinski.practice.record.DTO.RecordDTO;
import pl.sulinski.practice.record.DTO.UpdateRecordForm;
import pl.sulinski.practice.record.service.RecordService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/record")
@RequiredArgsConstructor
public class RecordController {

    private final RecordService recordService;

    @PostMapping
    public RecordDTO saveRecord(@RequestParam final UUID projectUuid, @RequestParam final UUID userUuid,
                                @RequestBody @Valid final AddRecordForm addRecordForm) {
        return recordService.saveRecord(projectUuid, userUuid, addRecordForm);
    }

    @DeleteMapping
    public RecordDTO deleteRecord(@RequestParam final UUID recordUuid, @RequestParam final UUID userUuid) {
        return recordService.deleteRecord(recordUuid, userUuid);
    }

    @GetMapping("/all")
    public List<RecordDTO> findAll(@RequestParam final UUID userUuid) {
        return recordService.findAll(userUuid);
    }

    @PutMapping
    public RecordDTO updateRecord(@RequestParam final UUID recordUuid, @RequestParam final UUID userUuid,
                                  @RequestBody @Valid final UpdateRecordForm updateRecordForm) {
        return recordService.updateRecord(recordUuid, userUuid, updateRecordForm);
    }
}
