package pl.sulinski.practice.user.DTO;

import pl.sulinski.practice.user.entity.WorkerType;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public record UpdateUserForm(
        @NotBlank(message = "login cannot be blank")
        String login,
        @NotBlank(message = "name cannot be blank")
        String name,
        @NotBlank(message = "lastName cannot be blank")
        String lastName,
        @NotNull(message = "workerType cannot be null")
        WorkerType workerType,
        @NotBlank(message = "password cannot be blank")
        String password,
        @Email
        @NotBlank
        String email,
        @NotNull
        @Positive
        @Max(99999999)
        BigDecimal costPerHour) {
}
