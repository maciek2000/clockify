package pl.sulinski.practice.user.entity;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import pl.sulinski.practice.common.entity.AbstractEntity;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.record.entity.Record;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_table")
@FieldNameConstants
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class User extends AbstractEntity {

    @Column(length = 255, unique = true, nullable = false, name = "login")
    private String login;
    @Column(length = 255, nullable = false, name = "name")
    private String name;
    @Column(length = 255, nullable = false, name = "last_name")
    private String lastName;
    @Enumerated(EnumType.STRING)
    private WorkerType workerType;
    @Column(length = 255, nullable = false, name = "password")
    private String password;
    @Column(unique = true, length = 255, nullable = false, name = "email")
    private String email;
    @Column(nullable = false, name = "cost_per_hour")
    private BigDecimal costPerHour;
    @ManyToMany(mappedBy = "users")
    private Set<Project> projects = new HashSet<>();
    @OneToMany(mappedBy = "user")
    private Set<Record> records = new HashSet<>();

    public void addProject(final Project project) {
        projects.add(project);
    }

    public void removeProject(final Project project) {
        projects.remove(project);
    }

    public void addRecord(final Record record) {
        record.setUser(this);
        records.add(record);
    }

    public void removeRecord(final Record record) {
        record.setUser(null);
        records.remove(record);
    }
}
