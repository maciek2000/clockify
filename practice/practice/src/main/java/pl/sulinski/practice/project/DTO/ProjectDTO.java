package pl.sulinski.practice.project.DTO;

import lombok.Builder;
import lombok.Getter;
import pl.sulinski.practice.user.DTO.UserDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Builder
public class ProjectDTO {

    private final String name;
    private final String description;
    private final LocalDate durationFrom;
    private final LocalDate durationTo;
    private final BigDecimal budget;
    private BigDecimal percentageBudgetUsage;
    private final Set<UserDTO> users;

    public ProjectDTO setPercentageBudgetUsage(final BigDecimal percentageBudgetUsage){
        this.percentageBudgetUsage = percentageBudgetUsage;
        return this;
    }
}
