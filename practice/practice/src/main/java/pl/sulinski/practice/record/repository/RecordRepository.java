package pl.sulinski.practice.record.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.sulinski.practice.record.entity.Record;
import pl.sulinski.practice.reports.project.DTO.ProjectReportProjection;
import pl.sulinski.practice.reports.user.DTO.UserReportProjection;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RecordRepository extends JpaRepository<Record, Long> {

    List<Record> findByUserUuid(final UUID uuid);

    Optional<Record> findByUuidAndUserUuid(final UUID recordUuid, final UUID userUuid);

    @Query(nativeQuery = true, value =
            "select p.name as name, " +
                    " sum(extract(hour from (r.end_time - r.start_time))) as hours," +
                    " sum(r.record_cost) as cost " +
                    "from record r " +
                    "left join user_table ut on (r.user_id = ut.id) " +
                    "left join public.project p on r.project_id = p.id " +
                    "where ut.uuid = ?1 " +
                    "and r.start_time between ?2 and ?3 " +
                    "group by p.name;")
    List<UserReportProjection> findByUserUuidAndDateRange(final UUID userUuid, final LocalDateTime lowerDateRange,
                                                          final LocalDateTime upperDateRange);

    @Query(nativeQuery = true, value =
            "select ut.login as login," +
                    " sum(extract(hour from (r.end_time - r.start_time))) as hours," +
                    " sum(r.record_cost) as cost " +
                    "from record r " +
                    "left join user_table ut on (r.user_id = ut.id) " +
                    "left join public.project p on r.project_id = p.id " +
                    "where p.uuid = ?1 " +
                    "and r.start_time between ?2 and ?3 " +
                    "group by ut.login;")
    List<ProjectReportProjection> findByProjectUuidAndDateRange(final UUID projectUuid, final LocalDateTime lowerDateRange,
                                                                final LocalDateTime upperDateRange);

}


