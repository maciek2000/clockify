package pl.sulinski.practice.reports.user.DTO;

import java.math.BigDecimal;

public interface UserReportProjection {
    BigDecimal getHours();

    BigDecimal getCost();

    String getName();
}
