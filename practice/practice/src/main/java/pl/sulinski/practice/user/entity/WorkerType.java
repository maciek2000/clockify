package pl.sulinski.practice.user.entity;

public enum WorkerType {
    EMPLOYEE, ADMINISTRATOR, MANAGER
}
