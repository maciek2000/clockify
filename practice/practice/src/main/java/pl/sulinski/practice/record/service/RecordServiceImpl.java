package pl.sulinski.practice.record.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.project.repository.ProjectRepository;
import pl.sulinski.practice.project.service.ProjectService;
import pl.sulinski.practice.record.DTO.AddRecordForm;
import pl.sulinski.practice.record.DTO.RecordDTO;
import pl.sulinski.practice.record.DTO.UpdateRecordForm;
import pl.sulinski.practice.record.entity.Record;
import pl.sulinski.practice.record.mapper.RecordMapper;
import pl.sulinski.practice.record.repository.RecordRepository;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.service.UserService;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RecordServiceImpl implements RecordService {

    private final ProjectRepository projectRepository;
    private final RecordRepository recordRepository;
    private final ProjectService projectService;
    private final UserService userService;

    @Override
    @Transactional
    public RecordDTO saveRecord(final UUID projectUuid, final UUID userUuid,
                                final AddRecordForm addRecordForm) throws NotFoundException {
        final User user = userService.findUser(userUuid);
        final Project project = projectRepository.findByUuidAndUsersUuid(projectUuid, userUuid)
                .orElseThrow(() -> new NotFoundException("User: " + userUuid + " dont belong to project: "
                        + projectUuid));
        final Record record = RecordMapper.toRecordFromAddRecordForm(addRecordForm, project, user);
        record.setRecordCost(calculateRecordCost(record));
        recordRepository.save(record);
        project.addRecord(record);
        user.addRecord(record);
        project.setPercentageBudgetUsage(projectService.calculatePercentageBudgetUsage(project));
        return RecordMapper.toDto(record);
    }

    @Override
    @Transactional
    public RecordDTO deleteRecord(final UUID recordUuid, final UUID userUuid) throws NotFoundException {
        final Record record = findRecordByUuidAndUserUuid(recordUuid, userUuid);
        final Project project = record.getProject();
        final User user = userService.findUser(userUuid);
        recordRepository.delete(record);
        final RecordDTO recordDTO = RecordMapper.toDto(record);
        project.removeRecord(record);
        user.removeRecord(record);
        project.setPercentageBudgetUsage(projectService.calculatePercentageBudgetUsage(project));
        return recordDTO;
    }

    @Override
    public List<RecordDTO> findAll(final UUID userUuid) throws NotFoundException {
        final List<Record> records = recordRepository.findByUserUuid(userUuid);

        return records.stream()
                .map(RecordMapper::toDto)
                .toList();
    }

    @Override
    @Transactional
    public RecordDTO updateRecord(final UUID recordUuid, final UUID userUuid, final UpdateRecordForm updateRecordForm) throws NotFoundException {
        final Record record = findRecordByUuidAndUserUuid(recordUuid, userUuid);

        final Project project = projectRepository.findByUuidAndUsersUuid(updateRecordForm.projectUuid(), userUuid)
                .orElseThrow(() -> new NotFoundException("User: " + userUuid + " dont belong to project: "
                        + updateRecordForm.projectUuid()));

        record.setStartTime(updateRecordForm.startTime());
        record.setEndTime(updateRecordForm.endTime());
        record.setTaskDescription(updateRecordForm.taskDescription());
        record.setProject(project);
        record.setRecordCost(calculateRecordCost(record));
        recordRepository.save(record);
        project.setPercentageBudgetUsage(projectService.calculatePercentageBudgetUsage(project));
        return RecordMapper.toDto(record);
    }

    @Override
    public Record findRecordByUuidAndUserUuid(final UUID recordUuid, final UUID userUuid) {
        return recordRepository.findByUuidAndUserUuid(recordUuid, userUuid)
                .orElseThrow(() -> new NotFoundException("User: " + userUuid + " dont have record: " + recordUuid));
    }

    private BigDecimal calculateRecordCost(final Record record){
        return BigDecimal.valueOf(ChronoUnit.HOURS.between(record.getStartTime(), record.getEndTime()))
                .multiply(record.getUser().getCostPerHour());
    }
}
