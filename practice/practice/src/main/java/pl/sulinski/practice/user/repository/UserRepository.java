package pl.sulinski.practice.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.user.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    Optional<User> findByUuid(UUID uuid) throws NotFoundException;

    boolean existsByEmail(final String email);

    boolean existsByLogin(final String login);
}
