package pl.sulinski.practice.record.entity;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import pl.sulinski.practice.common.entity.AbstractEntity;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.user.entity.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "record")
@FieldNameConstants
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class Record extends AbstractEntity {

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;
    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;
    @Column(name = "task_description")
    private String taskDescription;
    @Column(name = "record_cost", nullable = false)
    private BigDecimal recordCost;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

}
