package pl.sulinski.practice.project.entity;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import pl.sulinski.practice.common.entity.AbstractEntity;
import pl.sulinski.practice.record.entity.Record;
import pl.sulinski.practice.user.entity.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project")
@FieldNameConstants
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class Project extends AbstractEntity {

    @Column(length = 255, unique = true, nullable = false, name = "name")
    private String name;
    @Column(length = 255, name = "description")
    private String description;
    @Column(length = 255, nullable = false, name = "duration_from")
    private LocalDate durationFrom;
    @Column(length = 255, nullable = false, name = "duration_to")
    private LocalDate durationTo;
    @Column(nullable = false, name = "budget")
    private BigDecimal budget;
    @Column(nullable = false, name = "percentage_budget_usage")
    @Builder.Default
    private BigDecimal percentageBudgetUsage = BigDecimal.ZERO;

    @Builder.Default
    @ManyToMany(
            cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "project_user",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> users = new HashSet<>();
    @OneToMany(mappedBy = "project")
    private Set<Record> records = new HashSet<>();

    public void addUser(final User user) {
        users.add(user);
        user.addProject(this);
    }

    public void removeUser(final User user) {
        users.remove(user);
        user.removeProject(this);
    }

    public void addRecord(final Record record) {
        record.setProject(this);
        records.add(record);
    }

    public void removeRecord(final Record record) {
        record.setProject(null);
        records.remove(record);
    }
}

