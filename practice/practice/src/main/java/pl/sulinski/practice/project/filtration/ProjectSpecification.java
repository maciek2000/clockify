package pl.sulinski.practice.project.filtration;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import pl.sulinski.practice.common.specification.SpecificationUtils;
import pl.sulinski.practice.project.entity.Project;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static java.util.Objects.nonNull;

@AllArgsConstructor
public class ProjectSpecification implements Specification<Project> {

    private final ProjectFilterForm projectFilterForm;

    @Override
    public Predicate toPredicate(final Root<Project> root, final CriteriaQuery<?> query,
                                 final CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicateList = new ArrayList<>();

        SpecificationUtils.isLike(criteriaBuilder, root.get(Project.Fields.name), projectFilterForm.getName(),
                predicateList);
        isOverBudget(criteriaBuilder, root.get(Project.Fields.percentageBudgetUsage), predicateList,
                projectFilterForm.isOverBudget());
        SpecificationUtils.isDateBetween(criteriaBuilder, root.get(Project.Fields.durationFrom),
                root.get(Project.Fields.durationTo), predicateList, projectFilterForm.getDateFrom(),
                projectFilterForm.getDateTo());
        isUserIn(root, predicateList, projectFilterForm.getUuids());

        return criteriaBuilder.and(predicateList.toArray(Predicate[]::new));
    }

    public static void isOverBudget(final CriteriaBuilder criteriaBuilder, final Path<BigDecimal> path,
                                     final List<Predicate> predicateList, final boolean overBudget) {
        if (overBudget) {
            predicateList.add(criteriaBuilder.greaterThan(path, BigDecimal.valueOf(100)));
        }
    }

    public static void isUserIn(final Root<Project> root, final List<Predicate> predicateList,
                                final Set<UUID> uuids) {
        if (nonNull(uuids)) {
            predicateList.add(root.join("users").get("uuid").in(uuids));
        }
    }
}
