package pl.sulinski.practice.reports.user.service;

import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.user.DTO.UserReportDTO;

import java.util.UUID;

public interface UserReportService {

    UserReportDTO searchUserData(UUID managerUuid, UUID userUuid, TimePeriod timePeriod);
}
