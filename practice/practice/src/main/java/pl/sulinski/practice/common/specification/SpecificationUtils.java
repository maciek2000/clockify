package pl.sulinski.practice.common.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class SpecificationUtils {

    public static void isLike(final CriteriaBuilder criteriaBuilder, final Path<String> path,
                              final String value, final List<Predicate> predicateList) {
        if (nonNull(value)) {
            predicateList.add(criteriaBuilder.like(criteriaBuilder.lower(path),
                    "%" + value.toLowerCase() + "%"));
        }
    }

    public static void isBetween(final CriteriaBuilder criteriaBuilder, final Path<BigDecimal> path,
                                 final List<Predicate> predicateList, final BigDecimal lowBound,
                                 final BigDecimal topBound) {
        if (nonNull(lowBound) && nonNull(topBound)) {
            predicateList.add(criteriaBuilder.between(path, lowBound, topBound));
        }
        if (isNull(lowBound) && nonNull(topBound)) {
            predicateList.add(criteriaBuilder.between(path, BigDecimal.ZERO, topBound));
        }
        if (nonNull(lowBound) && isNull(topBound)) {
            predicateList.add(criteriaBuilder.between(path, lowBound, BigDecimal.valueOf(99999999.99)));
        }
    }

    public static <T> void isEqual(final CriteriaBuilder criteriaBuilder, final Path<T> path,
                                   final List<Predicate> predicateList, final T value) {
        if (nonNull(value)) {
            predicateList.add(criteriaBuilder.equal(path, value));
        }
    }


    public static void isDateBetween(final CriteriaBuilder criteriaBuilder, final Path<LocalDate> pathFrom,
                                     final Path<LocalDate> pathTo, final List<Predicate> predicateList,
                                     final LocalDate dateFrom, final LocalDate dateTo) {

        if (nonNull(dateFrom) && nonNull(dateTo)) {
            predicateList.add(criteriaBuilder.between(pathFrom, dateFrom, dateTo));
            predicateList.add(criteriaBuilder.between(pathTo, dateFrom, dateTo));
        }
        if (nonNull(dateFrom) && isNull(dateTo)) {
            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(pathFrom, dateFrom));
        }
        if (isNull(dateFrom) && nonNull(dateTo)) {
            predicateList.add(criteriaBuilder.lessThanOrEqualTo(pathTo, dateTo));
        }
    }

}
