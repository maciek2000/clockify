package pl.sulinski.practice.reports.user.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sulinski.practice.common.exception.AccessDeniedException;
import pl.sulinski.practice.project.service.ProjectService;
import pl.sulinski.practice.record.repository.RecordRepository;
import pl.sulinski.practice.reports.common.DTO.ReportDetail;
import pl.sulinski.practice.reports.common.period.TimePeriod;
import pl.sulinski.practice.reports.common.service.AbstractReportService;
import pl.sulinski.practice.reports.user.DTO.UserReportDTO;
import pl.sulinski.practice.reports.user.DTO.UserReportProjection;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserReportServiceImpl extends AbstractReportService implements UserReportService {

    private final RecordRepository recordRepository;
    private final ProjectService projectService;

    @Override
    public UserReportDTO searchUserData(final UUID managerUuid, final UUID userUuid, final TimePeriod timePeriod) {
        if (!projectService.hasRoleManager(managerUuid)) {
            throw new AccessDeniedException(managerUuid);
        }

        final LocalDateTime lowerDateRange = getLowerDateRange(timePeriod).atStartOfDay();
        final LocalDateTime upperDateRange = getUpperDateRange(timePeriod).atStartOfDay();

        final List<UserReportProjection> queryResult = recordRepository.findByUserUuidAndDateRange(userUuid,
                lowerDateRange, upperDateRange);

        final UserReportDTO userReportDTO = new UserReportDTO();

        queryResult.forEach(project -> calculateTotalUserTimeAndCost(project, userReportDTO));

        return userReportDTO;
    }

    private void calculateTotalUserTimeAndCost(final UserReportProjection project, final UserReportDTO userReportDTO) {
        userReportDTO.setSummaryCost(userReportDTO.getSummaryCost().add(project.getCost()));
        userReportDTO.setSummaryTime(userReportDTO.getSummaryTime().add(project.getHours()));
        userReportDTO.getUserProjects().add(new ReportDetail(project.getName(), project.getCost(), project.getHours()));
    }

}