package pl.sulinski.practice.reports.project.DTO;

import java.math.BigDecimal;

public interface ProjectReportProjection {
    String getLogin();

    BigDecimal getHours();

    BigDecimal getCost();
}
