CREATE SEQUENCE IF NOT EXISTS hibernate_sequence;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS project_user;
DROP TABLE IF EXISTS record;
DROP TABLE IF EXISTS user_table;
DROP TABLE IF EXISTS project;

CREATE TABLE IF NOT EXISTS "user_table"
(
    id            BIGINT                          NOT NULL PRIMARY KEY,
    uuid          UUID DEFAULT uuid_generate_v4() NOT NULL UNIQUE,
    login         VARCHAR(255)                    NOT NULL UNIQUE,
    name          VARCHAR(255)                    NOT NULL,
    last_name     VARCHAR(255)                    NOT NULL,
    worker_type   VARCHAR(13)                     NOT NULL CHECK (worker_type IN ('EMPLOYEE', 'ADMINISTRATOR', 'MANAGER')),
    password      VARCHAR(255)                    NOT NULL,
    email         VARCHAR(255)                    NOT NULL UNIQUE,
    cost_per_hour NUMERIC(10, 2)                  NOT NULL CHECK (cost_per_hour > 0)
);

CREATE TABLE IF NOT EXISTS "project"
(
    id                      BIGINT                                    NOT NULL PRIMARY KEY,
    uuid                    UUID           DEFAULT uuid_generate_v4() NOT NULL UNIQUE,
    name                    VARCHAR(255)                              NOT NULL UNIQUE,
    description             VARCHAR(255),
    duration_from           DATE                                      NOT NULL,
    duration_to             DATE                                      NOT NULL,
    budget                  NUMERIC(10, 2)                            NOT NULL CHECK (budget > 0),
    percentage_budget_usage NUMERIC(10, 2) DEFAULT 0                  NOT NULL
);

CREATE TABLE IF NOT EXISTS "project_user"
(
    project_id BIGINT REFERENCES project,
    user_id    BIGINT REFERENCES user_table,
    PRIMARY KEY (project_id, user_id)
);

CREATE TABLE IF NOT EXISTS "record"
(
    id               BIGINT                          NOT NULL PRIMARY KEY,
    uuid             UUID DEFAULT uuid_generate_v4() NOT NULL UNIQUE,
    start_time       TIMESTAMP                       NOT NULL,
    end_time         TIMESTAMP                       NOT NULL,
    task_description VARCHAR(255),
    record_cost      NUMERIC(5, 2)                   NOT NULL,
    user_id          BIGINT REFERENCES user_table (id),
    project_id       BIGINT REFERENCES project (id)
);

CREATE INDEX IF NOT EXISTS idx_project ON record (project_id);
CREATE INDEX IF NOT EXISTS idx_user ON record (user_id);

INSERT INTO user_table(id, login, name, last_name, worker_type, password, email, cost_per_hour)
VALUES ((SELECT nextval('hibernate_sequence')), 'maciek243', 'Maciej',
        'Sulinski', 'ADMINISTRATOR', 'tajnehaslo', 'macieksulinski@gmail.com', 29.4),
       ((SELECT nextval('hibernate_sequence')), 'filip768', 'Filip',
        'Onufrowicz', 'MANAGER', 'tajnehaslo2', 'filiponugrowicz@wp.pl', 32.4),
       ((SELECT nextval('hibernate_sequence')), 'krystynka12', 'Krystyna',
        'Malolepsza', 'EMPLOYEE', 'tajnehaslo3', 'krystynka@onet.pl', 42),
       ((SELECT nextval('hibernate_sequence')), 'hiero56', 'Hieronim',
        'Techner', 'EMPLOYEE', 'tajnehaslo3', 'hiero.techner@gmial.com', 32.4);

INSERT INTO project(id, name, description, duration_from, duration_to, budget)
VALUES ((SELECT nextval('hibernate_sequence')), 'Project1', 'Description project 1', '2022-03-20', '2022-08-25',
        150000),
       ((SELECT nextval('hibernate_sequence')), 'Project2', 'Description project 2', '2021-05-15', '2021-09-26',
        195000);

select * from project;