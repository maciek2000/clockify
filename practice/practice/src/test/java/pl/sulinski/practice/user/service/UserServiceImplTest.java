package pl.sulinski.practice.user.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sulinski.practice.common.exception.AccessDeniedException;
import pl.sulinski.practice.common.exception.AlreadyExistsException;
import pl.sulinski.practice.common.exception.NotFoundException;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.user.DTO.AddUserForm;
import pl.sulinski.practice.user.DTO.UpdateUserForm;
import pl.sulinski.practice.user.DTO.UserDTO;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.entity.WorkerType;
import pl.sulinski.practice.user.mapper.UserMapper;
import pl.sulinski.practice.user.repository.UserRepository;

import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.ListAssert.assertThatList;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private static User adminUser;
    private static User employeeUser;

    @BeforeEach
    private void init() {
        adminUser = User.builder()
                .login("maciek256")
                .name("Maciej")
                .lastName("Sulinski")
                .workerType(WorkerType.ADMINISTRATOR)
                .password("tajnehaslo")
                .email("macieksulinski18gmail.com")
                .costPerHour(BigDecimal.valueOf(30.5))
                .build();
        employeeUser = User.builder()
                .login("krystian398")
                .name("Krystian")
                .lastName("Karczynski")
                .workerType(WorkerType.EMPLOYEE)
                .password("tajnehaslo2")
                .email("krystian2@wp.pl")
                .costPerHour(BigDecimal.valueOf(20.5))
                .build();
    }

    @Test
    void shouldReturnAllUsers() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        when(userRepository.findAll()).thenReturn(List.of(adminUser, employeeUser));
        //when
        final List<UserDTO> actual = userService.findAll(adminUuid);
        //then
        assertThat(actual.size()).isEqualTo(2);
        assertThatList(actual).containsExactlyInAnyOrder(
                UserMapper.toDto(adminUser), UserMapper.toDto(employeeUser));
    }

    @Test
    void shouldThrowAccessDeniedExceptionWhenEmployeeWantToFindAll() {
        //given
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        //when//then
        assertThrows(AccessDeniedException.class,
                () -> userService.findAll(employeeUuid),
                "No needed role for user: " + employeeUuid);
    }

    @Test
    void shouldThrowNotFoundExceptionWhenUserWithNonExistingUuidWantToFindAll() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        given(userRepository.findByUuid(adminUuid)).willReturn(Optional.empty());
        //when//then
        assertThrows(NotFoundException.class,
                () -> userService.findAll(adminUuid),
                "User not found, uuid: " + adminUuid);
    }

    @Test
    void shouldSaveUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        final AddUserForm addUserForm = new AddUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        //when
        final UserDTO userDTO = userService.saveUser(adminUuid, addUserForm);
        //then
        assertThat(userDTO.login()).isEqualTo(employeeUser.getLogin());
    }

    @Test
    void shouldThrowAccessDeniedExceptionWhenEmployeeWantToSaveUser() {
        //given
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        final AddUserForm addUserForm = new AddUserForm("bartek2829", "Bartek", "Wandachowicz",
                WorkerType.EMPLOYEE, "tajnehaslo3", "bartek.wandachowicz@wp.pl",
                BigDecimal.valueOf(25.5));
        //when//then
        assertThrows(AccessDeniedException.class,
                () -> userService.saveUser(employeeUuid, addUserForm),
                "No needed role for user: " + employeeUuid);
    }

    @Test
    void shouldThrowNotFoundExceptionWhenUserWithNonExistingUuidWantToSaveUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        given(userRepository.findByUuid(adminUuid)).willReturn(Optional.empty());
        final AddUserForm addUserForm = new AddUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        //when//then
        assertThrows(NotFoundException.class,
                () -> userService.saveUser(adminUuid, addUserForm),
                "User not found, uuid: " + adminUuid);
    }

    @Test
    void shouldThrowAlreadyExistsExceptionWhenAdminWantToSaveUserWithExistingEmail() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        final AddUserForm addUserForm = new AddUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        when(userRepository.existsByEmail(addUserForm.email())).thenReturn(true);
        //when//then
        assertThrows(AlreadyExistsException.class,
                () -> userService.saveUser(adminUuid, addUserForm),
                "Email already exists, uuid: " + adminUuid);
    }

    @Test
    void shouldThrowAlreadyExistsExceptionWhenAdminWantToSaveUserWithExistingLogin() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        final AddUserForm addUserForm = new AddUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        when(userRepository.existsByLogin(addUserForm.login())).thenReturn(true);
        //when//then
        assertThrows(AlreadyExistsException.class,
                () -> userService.saveUser(adminUuid, addUserForm),
                "Login already exists, uuid: " + adminUuid);
    }

    @Test
    void shouldDeleteUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        //when
        final UserDTO userDTO = userService.deleteUserByUuid(adminUuid, employeeUuid);
        //then
        assertThat(userDTO.login()).isEqualTo(employeeUser.getLogin());
    }

    @Test
    void shouldThrowAccessDeniedExceptionWhenEmployeeWantToDeleteUser() {
        //given
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        //when//then
        assertThrows(AccessDeniedException.class,
                () -> userService.deleteUserByUuid(employeeUuid, adminUser.getUuid()),
                "No needed role for user: " + employeeUuid);
    }

    @Test
    void shouldThrowNotFoundExceptionWhenUserWithNonExistingUuidWantToDeleteUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        given(userRepository.findByUuid(adminUuid)).willReturn(Optional.empty());
        //when//then
        assertThrows(NotFoundException.class,
                () -> userService.deleteUserByUuid(adminUuid, employeeUser.getUuid()),
                "User not found, uuid: " + adminUuid);
    }

    @Test
    void shouldUpdateUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        final UpdateUserForm updateUserForm = new UpdateUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(45.2));
        //when
        final UserDTO userDto = userService.updateUser(adminUuid, employeeUuid, updateUserForm);
        //then
        assertThat(userDto.login()).isEqualTo("krystian398");
        assertThat(userDto.costPerHour()).isEqualTo(BigDecimal.valueOf(45.2));
    }

    @Test
    void shouldThrowAccessDeniedExceptionWhenEmployeeWantToUpdateUser() {
        //given
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        final UpdateUserForm updateUserForm = new UpdateUserForm("krystian3923", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "haslohaslo",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        //when//then
        assertThrows(AccessDeniedException.class,
                () -> userService.updateUser(employeeUuid, employeeUuid, updateUserForm),
                "No needed role for user: " + employeeUuid);
    }

    @Test
    void shouldThrowNotFoundExceptionWhenUserWithNonExistingUuidWantToUpdateUser() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        given(userRepository.findByUuid(adminUuid)).willReturn(Optional.empty());
        final UpdateUserForm updateUserForm = new UpdateUserForm("krystian3923", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "haslohaslo",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        //when//then
        assertThrows(NotFoundException.class,
                () -> userService.updateUser(adminUuid, employeeUser.getUuid(), updateUserForm),
                "User not found, uuid: " + adminUuid
        );
    }

    @Test
    void shouldThrowAlreadyExistsExceptionWhenAdminWantToUpdateUserWithExistingLogin() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        final UpdateUserForm updateUserForm = new UpdateUserForm("krystian578", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        when(userRepository.existsByLogin(updateUserForm.login())).thenReturn(true);
        //when//then
        assertThrows(AlreadyExistsException.class,
                () -> userService.updateUser(adminUuid, employeeUuid, updateUserForm),
                "Login already exists, uuid: " + adminUuid);
    }

    @Test
    void shouldThrowAlreadyExistsExceptionWhenAdminWantToUpdateUserWithExistingEmail() {
        //given
        final UUID adminUuid = adminUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        when(userRepository.findByUuid(adminUuid)).thenReturn(Optional.ofNullable(adminUser));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        final UpdateUserForm updateUserForm = new UpdateUserForm("krystian398", "Krystian",
                "Karczynski", WorkerType.EMPLOYEE, "tajnehaslo2",
                "krystian@wp.pl", BigDecimal.valueOf(20.5));
        when(userRepository.existsByEmail(updateUserForm.email())).thenReturn(true);
        //when//then
        assertThrows(AlreadyExistsException.class,
                () -> userService.updateUser(adminUuid, employeeUuid, updateUserForm),
                "Email already exists, uuid: " + adminUuid);
    }
}