package pl.sulinski.practice.project.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sulinski.practice.project.entity.Project;
import pl.sulinski.practice.project.repository.ProjectRepository;
import pl.sulinski.practice.user.entity.User;
import pl.sulinski.practice.user.entity.WorkerType;
import pl.sulinski.practice.user.repository.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProjectServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private ProjectRepository projectRepository;

    @InjectMocks
    private ProjectServiceImpl projectService;

    private User managerUser;
    private User employeeUser;
    private Project project;
    private Set<User> users;
    private Set<Project> projects;


    @BeforeEach
    private void init() {
        projects = new HashSet<>();
        users = new HashSet<>();
        project = Project.builder()
                .name("ProjectX")
                .description("Project description")
                .durationFrom(LocalDate.of(2022, 10, 12))
                .durationTo(LocalDate.of(2023, 11, 23))
                .budget(BigDecimal.valueOf(250000))
                .build();
        managerUser = User.builder()
                .login("maciek256")
                .name("Maciej")
                .lastName("Sulinski")
                .workerType(WorkerType.MANAGER)
                .password("tajnehaslo")
                .email("macieksulinski18gmail.com")
                .costPerHour(BigDecimal.valueOf(30.5))
                .projects(projects)
                .build();
        employeeUser = User.builder()
                .login("krystian398")
                .name("Krystian")
                .lastName("Karczynski")
                .workerType(WorkerType.EMPLOYEE)
                .password("tajnehaslo2")
                .email("krystian2@wp.pl")
                .costPerHour(BigDecimal.valueOf(20.5))
                .projects(projects)
                .build();
        project.addUser(managerUser);
    }

    @Test
    void shouldAddUserToProject() {
        //given
        final UUID mangerUuid = managerUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        final UUID projectUuid = project.getUuid();
        when(projectRepository.findByUuid(projectUuid)).thenReturn(Optional.ofNullable(project));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        when(userRepository.findByUuid(mangerUuid)).thenReturn(Optional.ofNullable(managerUser));
        //when
        projectService.addUserToProject(mangerUuid, projectUuid, employeeUuid);
        //then
        assertThat(project.getUsers().size()).isEqualTo(2);
        assertThat(project.getUsers()).containsExactlyInAnyOrder(employeeUser, managerUser);
    }

    @Test
    void shouldDeleteUserFromProject() {
        //given
        final UUID mangerUuid = managerUser.getUuid();
        final UUID employeeUuid = employeeUser.getUuid();
        final UUID projectUuid = project.getUuid();
        when(projectRepository.findByUuid(projectUuid)).thenReturn(Optional.ofNullable(project));
        when(userRepository.findByUuid(employeeUuid)).thenReturn(Optional.ofNullable(employeeUser));
        when(userRepository.findByUuid(mangerUuid)).thenReturn(Optional.ofNullable(managerUser));
        //when
        projectService.deleteUserFromProject(mangerUuid, projectUuid, employeeUuid);
        //then
        assertThat(project.getUsers().size()).isEqualTo(1);
        assertThat(project.getUsers()).doesNotContain(employeeUser);
    }
}